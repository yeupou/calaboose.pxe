#!/bin/bash
#
# Copyright (c) 2012-2020 Mathieu Roy <yeupou--gnu.org>
#           http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA


# This use wimboot http://ipxe.org/wimboot
# As such, tftpd is not enough, an http server must serve this directory
# so $PREFIX must be defined. Silently exits if unset.
#PREFIX=http://URL/pxe/
[ ! $PREFIX ] && exit

# cd in the directory where this script resides, even if called from a symlink
# in /etc/cron.whatever/
ZERO=$0
if [ -L $0 ]; then ZERO=`readlink $0`; fi
cd `dirname $ZERO`

# check if general required files exists 
REQUIRED_FILES="wimboot"
for file in $REQUIRED_FILES; do
    if [ ! -e $file ]; then 
	echo "$file is missing. Please read http://ipxe.org/wimboot"
	exit
    fi
done

# recreate the ipxe-boot file
echo "#!ipxe" > ipxe-boot
MENU=""

# Assume each folder contains bootmgr, BCD, etc
# (bios install win7/10)
EXPECTED_FILES="bootmgr boot/bcd boot/boot.sdi sources/boot.wim"
BONUS_FILES="boot/fonts/segmono_boot.ttf boot/fonts/segoe_slboot.ttf boot/fonts/segoen_slboot.ttf boot/fonts/wgl4_boot.ttf"
for system in *; do
    if [ ! -d $system/ ]; then continue; fi

  
    echo ":win$system" >> ipxe-boot

    # wimboot is always required
    echo "kernel "$PREFIX"Win/wimboot gui" >> ipxe-boot
    
    # check expected file (non blocker)
    for file in $EXPECTED_FILES; do
	if [ ! -e $system/"$file" ]; then
	    echo "expected $system/$file not found"
	else
	    echo "initrd "$PREFIX"Win/$system/$file `basename $file`" >> ipxe-boot
	fi
    done
    # add bonus files (that could be required for gui mode of wimboot
    for file in $BONUS_FILES; do
	if [ -e $system/"$file" ]; then
	    echo "initrd "$PREFIX"Win/$system/$file `basename $file`" >> ipxe-boot
	fi
    done
    echo "boot" >> ipxe-boot
    MENU="$MENU\nitem win$system Install Windows $system"
done


# add the menu lines
echo "menu Windows boot images" >> ipxe-boot
echo -e "$MENU" >> ipxe-boot
echo "choose target && goto \${target}" >> ipxe-boot
echo "# EOF" >> ipxe-boot

# EOF
